'use strict';

var util = require('../util');

var gulp = require('gulp');
var gulpif = require('gulp-if');
var path = require('path');
var wiredep = require('wiredep').stream;
var $ = require('gulp-load-plugins')();

// Injects compiled CSS/JS/HTML files into the main index page using gulp-inject
// Also uses wiredep to include libs from bower_components
function inject(callback) {
  var injectStyles = gulp.src([
    path.join(config.paths.tmp, '/serve', config.paths.scripts, '/**/*.css')
  ], { read: false });

  var injectStylesOptions = {
    ignorePath: [config.paths.src, path.join(config.paths.tmp, '/serve')],
    addRootSlash: false
  };

  // Sort the JS files so Angular dependency injection doesn't freak out
  var injectScripts = gulp.src([
    path.join(config.paths.src, config.paths.scripts, '/**/*.js'),
    path.join(config.paths.tmp, '/serve/', config.paths.scripts, '/**/*.js'),
    path.join('!' + config.paths.src, config.paths.scripts, '/**/*.spec.js'),
    path.join('!' + config.paths.src, config.paths.scripts, '/**/*.mock.js'),
    path.join('!' + config.paths.tmp, '/serve/', config.paths.scripts, '/**/*.spec.js'),
    path.join('!' + config.paths.src, config.paths.vendor)
  ])
  .pipe($.angularFilesort())
  .on('error', util.errorHandler('angularFilesort'));

  var injectScriptsOptions = {
    ignorePath: [config.paths.src, path.join(config.paths.tmp, '/serve')],
    addRootSlash: false
  };

  // Non Bower third party templates
  var injectVendor = gulp.src(path.join(config.paths.tmp, '/serve', config.paths.vendor, '/**/*.js'), { read: false });
  var injectVendorOptions = {
    starttag: '<!-- inject:vendor -->',
    ignorePath: [config.paths.src, path.join(config.paths.tmp, '/serve')],
    addRootSlash: false
  };

  // Angular templateCache injection into index.html
  var injectTemplates = gulp.src(path.join(config.paths.tmp, '/serve/templates/templates.js'), { read: false });
  var injectTemplatesOptions = {
    starttag: '<!-- inject:templates -->',
    ignorePath: [config.paths.src, path.join(config.paths.tmp, '/serve')],
    addRootSlash: false
  };

  return gulp.src(path.join(config.paths.src, '/*.html'))
    .pipe(gulpif(util.fileExists('bower.json'), wiredep({ directory: 'bower_components' })))
    .on('error', util.errorHandler('wiredep'))
    .pipe($.inject(injectTemplates, injectTemplatesOptions))
    .pipe($.inject(injectStyles, injectStylesOptions))
    .pipe($.inject(injectVendor, injectVendorOptions))
    .pipe($.inject(injectScripts, injectScriptsOptions))
    .pipe($.preprocess({ context: { NODE_ENV: process.env.NODE_ENV } }))
    .pipe(gulp.dest(path.join(config.paths.tmp, '/serve')));
}

gulp.task('inject', ['scripts', 'vendor', 'styles', 'templates', 'env', 'images'], inject);

module.exports = inject;
